package base;
public class Quote {

    private String text;

    private String author;



    public Quote(String text, String author) {

        this.text = text;

        this.author = author;

    }



    public String getText() {

        return text;

    }



    public String getAuthor() {

        return author;

    }





public class Quatoes{

    static transient Quote[] quotes = {

        new Quote("Progress comes from the intelligent use of experience.", "Elbert Hubbard"),

        new Quote("No amount of experimentation can ever prove me right; a single experiment can prove me wrong.", "Albert Einstein"),
               
    };

}

}
