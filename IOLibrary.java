package base;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.net.InetAddress;

public final class IOLibrary {

    private static BufferedReader getBufferedReader() {
        return new BufferedReader(new InputStreamReader(System.in));
    }

    public static String getString() {
        BufferedReader reader = getBufferedReader();
        try {
            return reader.readLine();
        } catch (IOException e) {
            throw new RuntimeException("Error reading input", e);
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static final int OCTET_COUNT = 4;

    public static InetAddress getIPAddress() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        do {
            try {
                String input = reader.readLine();
                byte[] data = parseIPAddress(input);
                return Inet4Address.getByAddress(data);
            } catch (IOException | InvalidIPAddressException e) {
                
            }
        } while (true);
    }

    private static byte[] parseIPAddress(String input) throws InvalidIPAddressException {
        String[] chunks = input.split("\\.");

        if (chunks.length != OCTET_COUNT) {
            throw new InvalidIPAddressException("Invalid IP address format");
        }

        byte[] data = new byte[OCTET_COUNT];
        for (int i = 0; i < OCTET_COUNT; i++) {
            data[i] = parseOctet(chunks[i]);
        }

        return data;
    }

    private static byte parseOctet(String octet) throws InvalidIPAddressException {
        try {
            int value = Integer.parseInt(octet);
            if (value < 0 || value > 255) {
                throw new InvalidIPAddressException("Each octet must be between 0 and 255");
            }
            return (byte) value;
        } catch (NumberFormatException e) {
            throw new InvalidIPAddressException("Invalid octet format");
        }
    }

    public static class InvalidIPAddressException extends Exception {
        public InvalidIPAddressException(String message) {
            super(message);
        }
    }

    public static void main(String[] args) {
        InetAddress ipAddress = getIPAddress();
        System.out.println("IP Address: " + ipAddress.getHostAddress());
    }
}
