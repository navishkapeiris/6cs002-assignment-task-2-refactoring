package base;

import java.net.InetAddress;

public class ConnectionGenius {
     private InetAddress ipAddress;

    public ConnectionGenius(InetAddress ipAddress) {
        this.ipAddress = ipAddress;
    }
     public InetAddress getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(InetAddress ipAddress) {
        this.ipAddress = ipAddress;
    }

    public void fireUpGame() {
        downloadWebVersion();
        connectToWebService();
        awayWeGo();
    }

    public void downloadWebVersion() {
        System.out.println("Getting specialized web version.");
        System.out.println("Please wait a moment.");
    }

    public void connectToWebService() {
        System.out.println("Connecting to web service.");
    }

    public void awayWeGo() {
        System.out.println("Ready to play.");
    }

    public static void main(String[] args) {
        try {
            InetAddress ipAddress = InetAddress.getByName("example.com");
            ConnectionGenius genius = new ConnectionGenius(ipAddress);
            genius.fireUpGame();
            System.out.println(ipAddress + "\n" + genius);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
