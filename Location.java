package base;
import java.awt.Color;
import java.awt.Graphics;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * @author Kevan Buckley, maintained by __student
 * @version 2.0, 2014
 */

public class Location extends SpacePlace {
  public int c;
  public int r;
  public DIRECTION d;
  public int temperature; // Introducing an explaining variable

	  
  public int tmp;
  public enum DIRECTION {VERTICAL, HORIZONTAL};  
  public Location(int r, int c) {
    this.r = r;
    this.c = c;
  }

  public Location(int r, int c, DIRECTION d) {    
    this(r,c);
    this.d=d;
  }
  
  public String toString() {
    if(d==null){
      tmp = c + 1;
      return "(" + (tmp) + "," + (r+1) + ")";
    } else {
      tmp = c + 1;
      return "(" + (tmp) + "," + (r+1) + "," + d + ")";
    }
  }
  
  public void drawGridLines(Graphics g) {
	    drawLines(g, 0, 7, 20, 20, 20, 180);
	    drawLines(g, 0, 8, 20, 20, 20, 160);
	}

	private void drawLines(Graphics g, int start, int end, int base, int from, int step, int to) {
	    g.setColor(Color.LIGHT_GRAY);
	    for (int i = start; i <= end; i++) {
	        g.drawLine(base + i * from, base, base + i * step, to);
	    }
	}

  
  public static int getInt() {
    BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
    do {
      try {
        return Integer.parseInt(r.readLine());
      } catch (Exception e) {
      }
    } while (true);
  }
}
